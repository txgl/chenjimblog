
## Android视频添加水印、录制、播放


#### Mp4文件详解   
- Mp4文件头Box信息详解，文件恢复  
  <https://blog.csdn.net/HUAJUN998/article/details/115005667>  
  mp4info.exe 下载 <https://gitee.com/chenjim/chenjimblog/attach_files/875058/download/mp4info.zip>   

- H264的SPS、PPS解析  
  <https://blog.csdn.net/DaveBobo/article/details/51125353>  

- mp4格式转换、加解密、信息显示   
  <http://www.bento4.com/downloads>  
  <https://github.com/axiomatic-systems/Bento4>  

#### ffmpeg ffplay ffprobe 三剑客 


- ffprobe显示视频中video、audio编码信息等  
  `ffprobe.exe 1234.MP4`

####  WebRTC视频流渲染中插入图片帧  
  <https://www.jianshu.com/p/c126c4831e8b>  
  修改 `webrtc/modules/video_render/incoming_video_stream.cc` 中   
  `IncomingVideoStream::RenderFrame`   增加图片水印   
  缺点：需要生成图片数组，可扩展性低

#### 安卓视频加水印，录像，并保存MP4文件
    <https://mykb.cipindanci.com/archive/SuperKB/4113/>  
    Camera 的 `onPreviewFrame` 回调 YUV 转JPG， 再转Bitmap，加水印后
    Draw 到 SurfaceView，再录制 SurfaceView
    缺点：效率差。。。。


####  Webrtc 本地视频录制  
<https://stackoverflow.com/questions/40416166/how-to-record-webrtc-local-video-stream-to-file-on-android>  
- For accessing to raw video frames both, remote and local peers, see interface VideoSink. [apprtc/CallActivity.java](https://github.com/Piasy/AppRTC-Android/blob/master/app/src/main/java/org/appspot/apprtc/CallActivity.java) class ProxyVideoSink)   
- For getting audio samples see class [RecordedAudioToFileController](https://github.com/Piasy/AppRTC-Android/blob/master/app/src/main/java/org/appspot/apprtc/RecordedAudioToFileController.java)     
- For creating/muxing video files you can use [MediaMuxer](https://developer.android.com/reference/android/media/MediaMuxer) class from Android SDK.   
  以上获取 视频帧 `org.webrtc.VideoFrame` ， encode 可以参考 `org.webrtc.HardwareVideoEncoder`  
  


#### 将View Draw Bitmap后 MediaRecorder录制 (z4hyoung/ViewRecord)   
  <https://github.com/z4hyoung/ViewRecorder>   
  将 View.getDrawingCache() 获取到的 Bitmap Draw 到 Surface，  
  然后用 MediaRecorder 录制 ，
  缺点：较大Bitmap快速创建、绘制、释放效率，**效率特别低**   

####  用MediaProjection采集屏幕 (thfhongfeng/PineAppRtc )  
  <https://www.jianshu.com/p/7746c7411826> 需要在 WebRtcAudioTrack.java 添加音频回调   
  <https://github.com/thfhongfeng/PineAppRtc>   
  a) 视频数据采集使用MediaProjection+VirtualDisplay采集屏幕，不满足一般项目需求  
  b) 双路音频的混音使用平均算法,参见 `MediaRecordController.java` 中 函数`average`   
  c) 使用MediaCodec编码，使用MediaMuxer封装成MP4文件   
  原中引用的 `org.webrtc.voiceengine.*` 在新的代码中并未使用，  
  经验证使用的是 `sdk/android/src/java/org/webrtc/audio/` 中代码，在其中按照原文添加，可以获取到相应的 pcm  

#### 基于FFmpeg RTMP 推流相关示例讲解 

<https://github.com/EricLi22/AndroidMultiMedia>  

- [Linux下FFmpeg编译以及Android平台下使用](https://www.jianshu.com/p/4037297d883d)—[源码v1.0](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.0)

- [Android平台下使用FFmpeg进行RTMP推流（视频文件推流)](https://www.jianshu.com/p/dcac5da8f1da)—[源码v1.1](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.1)
- [Android平台下使用FFmpeg进行RTMP推流（摄像头推流）](https://www.jianshu.com/p/462e489b7ce0)—[源码v1.2.1](https://github.com/EricLi22/FFmpegSample/releases/tag/1.2.1)
- [Android RTMP推流之MediaCodec硬编码一（H.264进行flv封装）](https://www.jianshu.com/p/e607e63fb78f)—[源码v1.3](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.3)
- [Android平台下RTMPDump的使用](https://www.jianshu.com/p/3ee9e5e4d630)—[源码v1.4](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.4)
- [Android RTMP推流之MediaCodec硬编码二（RTMPDump推流）](https://www.jianshu.com/p/53ddf0831d2c)-[源码v1.5](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.5)
- [MediaCodec进行AAC编解码（文件格式转换）](https://www.jianshu.com/p/875049a5b40f)-[源码v1.6](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.6)
- [MediaCodec进行AAC编解码（AudioRecord采集编码）](https://www.jianshu.com/p/e32ec8a8df41)-[源码v1.7](https://github.com/EricLi22/FFmpegSample/releases/tag/v1.7)



[ADTSUtils](https://github.com/EricLi22/AndroidMultiMedia/blob/master/rtmpfile/src/main/java/com/wangheart/rtmpfile/utils/ADTSUtils.java)  AAC 添加帧头  


AAC ADTS格式分析  <https://zhuanlan.zhihu.com/p/162998699>   

AAC的ADTS头解析  <https://www.jianshu.com/p/b5ca697535bd>   

----

### Google grafika 相关解读  
  [android-encoding-using-mediacodec-and-a-surface](https://stackoverflow.com/questions/32439950/android-encoding-using-mediacodec-and-a-surface)  


Android之grafika源码阅读  
https://blog.csdn.net/I_do_can/article/details/69396461  


视频录制与滤镜（三）：grafika——Show + capture camera  
https://blog.csdn.net/jw20082009jw/article/details/80861179


### 视频倍速播放  
Android MediaPlayer在API 23即6.0以上支持了设置播放速率，参见 [链接](https://www.jianshu.com/p/510f2b134d93)  
```
private void changeplayerSpeed(float speed) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (player.isPlaying()) {
            player.setPlaybackParams(player.getPlaybackParams().setSpeed(speed));
        } else {
            player.setPlaybackParams(player.getPlaybackParams().setSpeed(speed));
            player.pause();
        }
    }
}
```
通过 [与时间赛跑--腾讯视频多倍速播放产品设计小结](https://zhuanlan.zhihu.com/p/25601930)  
我们看到 安卓在2倍蓝光视频部分设备播放时存在卡顿问题 

----

